import React from 'react';
import AmCharts from '@amcharts/amcharts3-react';
import _ from 'lodash';

function ExpenseInsights(props) {
    
    let insights = [
        { amount: 0, type: "food" },
        { amount: 0, type: "rent" },
        { amount: 0, type: "groceries"},
        { amount: 0, type: "entertainment"},
        { amount: 0, type: "other"}
    ]
    const calculateInsights = () => {
        let food, rent, groceries, entertainment, other;
          props.expenses.forEach((expense, i) => {
            switch(expense.type) {
              case "food":
                  food = _.find(insights, function(o) { return o.type === 'food'});
                  food.amount += expense.amount;
                  console.log(food);
                  break;
              case "rent": 
                  rent = _.find(insights, function(o) { return o.type === 'rent'});
                  rent.amount += expense.amount;
                  console.log(rent)
                  break;
              case "groceries": 
                  groceries = _.find(insights, function(o) { return o.type === 'groceries'});
                  groceries.amount += expense.amount;
                  console.log(groceries);
                  break;
              case "entertainment": 
                  entertainment = _.find(insights, function(o) { return o.type === 'entertainment'});
                  entertainment.amount += expense.amount;
                  console.log(entertainment);
                  break;
              default:
                    other = _.find(insights, function(o) { return o.type === 'other'});
                    other.amount += expense.amount;
            } 
          })
      }

    var chartOptions = {
        type: "pie",
        theme: "light",
        dataProvider: insights,
        valueField: "amount",
        titleField: "type",
        balloon: {
            fixedPosition: true
        }
      };
    
    calculateInsights();
    return(
        <div>
            <div className="heading">Insights</div>
            <AmCharts.React
                style={{ width: "100%", height: "400px" }}
                options={chartOptions}
            />
        </div>
    )
}

export default ExpenseInsights;