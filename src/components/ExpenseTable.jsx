import React from 'react';

function ExpenseTable(props) {

    return(
        <div>
            <div className="heading">Transactions</div>
            <table className="table-expense">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Category</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                {props.expenses.map((expense,i) => (
                    <tr key={i}>
                    <td>{expense.date}</td>
                    <td>{expense.amount}</td>
                    <td>{expense.type}</td>
                    <td>{expense.description}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    )
}

export default ExpenseTable;