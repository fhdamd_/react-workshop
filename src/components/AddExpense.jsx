import React from 'react';
import "./AddExpense.scss";

function AddExpense(props) {

    let expense = {};
    const addNewExpense = e => {
        e.preventDefault();
        props.addNewExpense(expense);
    }
    return(
        <div className="expense-form">
            <div className="heading">Add an Expense</div>
            <hr/>
            <form onSubmit={addNewExpense} id="expenseForm">
                <input type="text" value={expense.amount} placeholder="Enter Amount" onChange={ e => { expense.amount = parseFloat(e.target.value) }} />
                <input type="text" value={expense.type} placeholder="Enter category" onChange={ e => { expense.type = e.target.value }} />
                <input type="text" value={expense.description} placeholder="Enter description" onChange={ e => { expense.description = e.target.value }} />
                <input type="text" value={expense.date} placeholder="Enter date in DD/MM/YYYY format" onChange={ e => { expense.date = e.target.value }} />
                <button type="submit" className="expense-btn">Add Expense</button>
            </form>
        </div>
    )
}

export default AddExpense;