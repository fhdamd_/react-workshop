const expenses = [
    {
      amount: 4.0,
      type: "food",
      description: "Coffee",
      date: "17/10/2019"
    },
    {
      amount: 150.0,
      type: "rent",
      description: "House Rent",
      date: "15/10/2019"
    },
    {
      amount: 80.0,
      type: "groceries",
      description: "Coles shopping",
      date: "15/10/2019"
    },
    {
      amount: 11.0,
      type: "entertainment",
      description: "Spotify monthly subscription",
      date: "12/10/2019"
    }
];

export default expenses;