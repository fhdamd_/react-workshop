import React, {useState } from 'react';
import './App.css';

import ExpenseTable from './components/ExpenseTable.jsx';
import AddExpense from './components/AddExpense.jsx';
import ExpenseInsights from './components/ExpenseInsights.jsx';

import data from './data/expenses';

function App() {

  // expense types = food, housing, groceries, entertainment, transport, health, entertainment

  const [expenses, setExpenses] = useState(data);
  

  const addNewExpense = (expense) => {
    console.log(expense);
    setExpenses(prevExpenses => (
      [
        ...prevExpenses, 
        {
          amount: expense.amount, 
          type: expense.type, 
          description: expense.description, 
          date: expense.date 
        }
      ]
    ));
    
    document.getElementById("expenseForm").reset();
  }
  
  return (
    <div className="App">
      <div className="header">
        Expense Tracker
      </div>
      <div className="content">
        <AddExpense addNewExpense={addNewExpense} />
        <div className="insights">
          <ExpenseInsights expenses={expenses} />
          <ExpenseTable expenses={expenses} />
        </div>
      </div>
      
    </div>
  );
}

export default App;
